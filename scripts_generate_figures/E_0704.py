#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0704:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0704_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "52ad0749f41215aa68f124c12b328678"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=3)

        x = np.array(data[:, 0]) * 1e-6
        tot = np.nansum((data[:, 1], data[:, 2], data[:, 3], data[:, 4]), axis=0)

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x, tot)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("linear")
        p.plt.xlim(left=0.2, right=20)
        p.plt.ylim(bottom=0, top=1.5)
        p.plt.xticks([10**x for x in range(0, 2)])
        p.plt.yticks([0.5 * y for y in range(0, 4)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0704_Figure_01.png")
        return


def make_plots(destination):
    e0704 = E_0704(destination)
    logging.info(" Generating E704, Figure 1")
    e0704.plot_figure_01()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
