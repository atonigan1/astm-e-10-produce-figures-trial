E385
====

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 385.  The recommended citation for this standard is:

* ASTM E385-16, Standard Test Method for Oxygen Content Using a 14-MeV Neutron
  Activation and Direct-Counting Technique, ASTM International, West
  Conshohocken, PA, 2016, www.astm.org

