Docker
======

This document describes how to use a pre-configured Docker container to perform
common data-processing operations needed to maintain certain ASTM International
technical standards.  The advantage of using Docker rather than installing this
repository on the host machine is that the Docker image contains a recent image
of the operating system and required dependencies, and is regularly tested using
Gitlab.com-based continuous-integration (CI), such that there is effectively no
infrastructure-configuration overhead.

.. note::
   Installation of the Docker software is outside the scope of this document,
   and it assumes that the software is installed, functional, and configured
   such that Docker commands are available on the command line (i.e., in the
   user's ``PATH``).

Definitions
-----------

Docker
  An open-source software designed to enable easy application development and
  deployment.  One of the main benefits is that Docker provides a reproducible
  environment within which an application can be developed and deployed such
  that the host operating system configuration is irrelevant to
  developing/running the application(s) encapsulated in the Docker image.

image
  A read-only file that contains the source code, libraries, dependencies,
  tools, and other files needed for an application to run.

container
  A virtualized run-time environment isolated from the host system. These
  containers are usually compact and portable units within which an application
  can be started quickly and easily.  Containers are read/write environments
  that are constructed from an image (or possibly composed from multiple
  images).

Retrieving Docker Images from Gitlab.com
----------------------------------------

Gitlab.com can store Docker images in a project's Container Registry.  An
example of this is shown in :ref:`fig:Docker_01`.


.. _fig:Docker_01:
.. figure:: figures/Docker_01.png
   :width: 6.5in

   Example Gitlab.com Container Registry A Root Image with Two Tags

Clicking on the image (i.e., the Root Image) will show the tags within it.  In
this case, there are two tags as shown in :ref:`fig:Docker_02`:

``astmplotter``
  Contains the Python scripts to generate various ASTM International figures.
  The generated figures can be extracted from the resulting Docker container.

``njoy2016``
  Contains a built copy of `NJOY2016 <https://github.com/njoy/NJOY2016>`_ with
  modifications necessary to process nuclear data such as that needed by
  :ref:`e0693`.

.. _fig:Docker_02:
.. figure:: figures/Docker_02.png
   :width: 6.5in

   Two Tagged Docker Images

Finally, the details of each tagged image can be obtained by clicking on the
``...`` next to each tagged image, as shown in :ref:`fig:Docker_03`.

.. _fig:Docker_03:
.. figure:: figures/Docker_03.png
   :width: 6.5in

   Details of Two Tagged Docker Images

To retrieve a particular tagged image, click the "clipboard" button next to the
tagged image name to copy the associated URL to the system clipboard and
retrieve the desired Docker image with the command

.. code-block:: console

    docker pull registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures:astmplotter

or

.. code-block:: console

    docker pull registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures:njoy2016

The two aforementioned Docker images were built based on the `Ubuntu Docker
image <https://hub.docker.com/_/ubuntu>`_ using a Bash-based provisioning script
(e.g., the `astmplotter
<https://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures/-/blob/master/.gitlab/provision_astmplotter.sh>`_
or `njoy2016
<https://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures/-/blob/master/.gitlab/provision_njoy2016.sh>`_
scripts).  The actual Docker images are provisioned using Gitlab.com-hosted CI
using `a Gitlab CI YAML file
<https://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures/-/blob/master/.gitlab-ci.yml>`_;
however, one could also use such a provisioning script on a host computer
running the Ubuntu Linux distribution or as driven by a Dockerfile (e.g., the
`astmplotter
<https://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures/-/blob/master/.gitlab/Dockerfile_ubuntu_astmplotter>`_
or `njoy2016
<://gitlab.com/ASTM_E10_Data_Collator/ASTM_E10_Produce_Figures/-/blob/master/.gitlab/Dockerfile_ubuntu_njoy2016_rebased_pjgriff_mods>`_
Dockerfiles) to configure the host computer or a new Docker image, respectively.

Running an Interactive Docker Container
---------------------------------------

Using the pulled ``astmplotter`` Docker image as an example, one can execute it
interactively.  Before doing so, its unique identifier must be obtained.  This
is done with the command:

.. code-block:: console

   docker images --all

which will list all images present.  The unique identifier is listed as a
hexadecimal code as shown in the output:

.. code-block:: console

   REPOSITORY                                                            TAG           IMAGE ID       CREATED       SIZE
   registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures   njoy2016      69a3f0d4953b   3 weeks ago   368MB
   registry.gitlab.com/astm_e10_data_collator/astm_e10_produce_figures   astmplotter   5cefa0ae15ab   3 weeks ago   502MB

With the hexadecimal ID ``5cefa0ae15ab``, one can create a container and run it
interactively with the command:

.. code-block:: console

   docker run -it 5cefa0ae15ab bash

If successful, the prompt for the container should be shown such as:

.. code-block:: console

   root@51b1c88f8885:/#

Moving Data To and From a Container
-----------------------------------

Files can be moved between the host operating system and the Docker container
using the ``docker cp`` command.  This command is always executed on the host
operating system (because the Docker container is isolated from the host).

When specifying the container's location, the container name/ID is specified
along with the path, which are delimited by a colon.  For example, copying all
files in the current directory to the container's root directory is accomplished
with the command (by replacing ``<NAME>`` with the name of the container):

.. code-block::

   docker cp . <NAME>:/

To copy all files in the ``/doc/figures_generated`` directory back to the
current working directory on the host, one could use the command:

.. code-block::

   docker cp <NAME>:/doc/figures_generated/. .

where the trailing ``.`` on the source directory specifies to copy the contents
of the directory ``figures_generated`` and not the directory itself.

Collection of Cookbooks for Generating Plots
--------------------------------------------

The following code snippets are used to launch various Docker containers and
interact with them to ultimately generate a series of plots for ASTM
International Standards.  The code snippets are separated based on where
commands are being entered (the host operating system, the ``njoy2016`` Docker
container, or the ``astmplotter`` Docker container).

.. note::
   To follow these "cookbook" entries, it is assumed that

   * the host operating system  acts in a \*nix-like manner (is Linux, macOS,
     Windows System for Linux (WSL), etc.) using a Bash-compliant set of
     commands,
   * the ``git`` application is installed, functional, and available on the
     host operating system command line, and
   * all commands are executed using a single command-line environment (i.e., a
     single terminal application is launched on the host operating system and
     used until all work is complete).

   Furthermore, the host operating system environment variable ``$GIT`` is
   defined and used throughout these examples.  If the reader wishes to place the
   this directory elsewhere, this variable should be redefined.

Begin by working on the host operating system and entering the commands:

.. literalinclude:: include/Docker_Cookbook_01_Host.sh
   :language: console
   :lines: 3-
   :caption: Host Operating System

.. note::
   The hexadecimal code used to create the Docker container named
   ``run_njoy2016`` (and ``run_astmplotter`` below) come from the output of the
   ``docker images --all`` command.

Now, one should be within the ``run_njoy2016`` Docker container.  Commands to
run NJOY2016 follow:

.. literalinclude:: include/Docker_Cookbook_02_run_njoy2016.sh
   :language: console
   :lines: 3-
   :caption: ``run_njoy2016`` Docker Container

At this point, NJOY2016 should have executed and created the data files
necessary to generate.  They are copied back to the host operating system and
then copied into a container to actually produce the desired images.

.. literalinclude:: include/Docker_Cookbook_03_Host.sh
   :language: console
   :lines: 3-
   :caption: Host Operating System

Now, one should be within the ``run_astmplotter`` Docker container with the
repository contents copied into it.  To produces the figures within, run the
commands:

.. literalinclude:: include/Docker_Cookbook_04_run_astmplotter.sh
   :language: console
   :lines: 3-
   :caption: ``run_astmplotter`` Docker Container

At this point, the ``make_all_figures.py`` script should have executed and
populated the ``doc/figures_generated`` directory.  On the host operating
system, copy back the generated figures with the commands:

.. literalinclude:: include/Docker_Cookbook_05_Host.sh
   :language: console
   :lines: 3-
   :caption: Host Operating System

Now, the ``doc/figures_generated`` directory on the host operating system should
be populated with the figures that were just produced.
