.. _chap::Retrieving_Reaction_Product_Data:

Retrieving Reaction Product Data
================================

This chapter describes how to retrieve reaction-product data using web-based
approaches.

DDEP, LNHB, and BIPM
--------------------

The first approach is based on the online `DDEP
<http://www.nucleide.org/DDEP_WG/DDEPdata.htm>`_ repository, which has recently
been updated to a new website hosted by the Laboratoire National Henri Becquerel
on `Atomic and Nuclear Data
<http://www.lnhb.fr/nuclear-data/nuclear-data-table/>`_.  This website hosts
publications from the Bureau International des Poids and Mesures (BIPM) on
`Radiation Dosimetry <https://www.bipm.org/en/dosimetry>`_, which consists of
several volumes.  It also has an interactive online lookup tool for nuclides of
interest.  As such, the lookup tool can be used to download a PDF document (or
other files) that provides the data sought.

As an example, :superscript:`24`\ Na can be searched for as shown in
:ref:`fig::LNHB_Figure_01` and `the resulting PDF
<http://www.lnhb.fr/nuclides/Na-24_tables.pdf>`_ can be examined to get the
half-life and emission information needed for this table.

.. _fig::LNHB_Figure_01:
.. figure:: figures/LNHB_01.png
   :width: 6.5in

   Figure 1: Example Query of LNHB Atomic and Nuclear Data Website

.. _fig::LNHB_Figure_02:
.. figure:: figures/LNHB_02.png
   :width: 6.5in

   Figure 2: :superscript:`24`\ Na Half-life Duration

.. _fig::LNHB_Figure_03:
.. figure:: figures/LNHB_03.png
   :width: 6.5in

   Figure 3: :superscript:`24`\ Na Gamma Emission Energies and Intensities

From the resulting PDF in :ref:`fig::LNHB_Figure_02`, the half-life is 14.958(2)
hours and from :ref:`fig::LNHB_Figure_03`, the principal gamma emissions are
at 1368.630(5) and 2754.049(13) keV.

BNL NNDC ENSDF
--------------

However, some data are not available in the LNHB website PDFs.  In these cases,
ENSDF files can be interrogated directly, either available from the LNHB website
or from the `Brookhaven National Laboratory (BNL) National Nuclear Data Center
(NNDC) website <https://www.nndc.bnl.gov/ensdf>`_.

As an example, :superscript:`48`\ Sc information can be searched for as shown in
:ref:`` (noting that the search begins by seeking the target, :superscript:`48`\
Ti rather than the product) as shown in :ref:`fig::BNL_NNDC_Figure_01`.

.. _fig::BNL_NNDC_Figure_01:
.. figure:: figures/BNL_NNDC_ENSDF_01.png
   :width: 6.5in

   Figure 4: Example Query of BNL NNDC ENSDF Website

.. _fig::BNL_NNDC_Figure_02:
.. figure:: figures/BNL_NNDC_ENSDF_02.png
   :width: 6.5in

   Figure 5: Selecting Dataset of Interest

.. _fig::BNL_NNDC_Figure_03:
.. figure:: figures/BNL_NNDC_ENSDF_03.png
   :width: 6.5in

   Figure 6: :superscript:`48`\ Sc Half-life Duration

.. _fig::BNL_NNDC_Figure_04:
.. figure:: figures/BNL_NNDC_ENSDF_04.png
   :width: 6.5in

   Figure 7: :superscript:`48`\ Sc Gamma Emission Energies and Intensities

Once the appropriate data set is selected such as in
:ref:`fig::BNL_NNDC_Figure_02`, one can find the half-life of :superscript:`48`\
Sc to be 43.67(9) hours (:ref:`fig::BNL_NNDC_Figure_03`) with principal gamma
emissions at 983.526(12),
1037.522(12), and 1312.120(12) keV at intensities of 100.0(3), 97.5(5), and
100.0(5), respectively (:ref:`fig::BNL_NNDC_Figure_04`).

